<?php
/**
 * Created by PhpStorm.
 * User: rolpa
 * Date: 9.10.14
 * Time: 11:18
 */

namespace roland\Tool;

class Route
{

    protected $routes;

    public function __construct()
    {
        $this->routes = array();
    }

    public function addRoutes()
    {
        $conf = $this->getConfig();
        echo 'Mida asja';die;
        $method = 'ANY';
        foreach ($conf['routes'] as $route) {
            //@TODO $route['service'] this needs a check if it exists
            $r = new \Slim\Route($route['route'], $this->processCallback($route['resource']));
            $r->setHttpMethods($method);

            array_push($this->routes, $r);
        }

        return $this->routes;
    }

    protected function processCallback($resource)
    {
        $callback = function () use ($resource) {
            $class = new $resource();
            return $class;
        };
        return $callback;
    }


    public function getConfig()
    {
        return array(
            'routes' => array(
                'orders' => array(
                    'route' => '/orders(/:id)',
                    'resource' => '\\OrdersResource'
                ),

            ),
            'services' => array(
                '\\OrdersResource' => array(
                    'route_identifier' => 'id',
                    'resource_name' => 'orders',
                    'collection_methods' => array('GET', 'POST'),
                    'entity_methods' => array('GET', 'DELETE', 'PUT')
                ),
            )
        );
    }
}
