<?php
/**
 * Created by PhpStorm.
 * User: rolpa
 * Date: 2.10.14
 * Time: 12:14
 */

namespace roland\Tool\Views;

use Slim\Slim;
use Slim\View;
use Hal\Resource;

class RestFulView extends View
{
    /**
     * @var string What type the response should be
     */
    protected $responseType;

    /**
     * Are we in debug mode ?
     * @var boolean
     */
    private $debug;

    private $statusCode;

    public function __construct($debug = false, $responseType = 'json')
    {
        parent::__construct();
        $this->debug = $debug;
        $this->responseType = $responseType;
    }

    public function render($statusCode = 200, $data = null)
    {
        $app = Slim::getInstance();
        $response = $this->all();
        $this->statusCode = $statusCode;


        //@TODO This should rely on content negotiator
        $app->response->header('Content-Type', 'application/json');

        if ($this->responseType == 'json') {
            // This is not needed
            unset($response['flash']);
        }

        $body = $this->makeHalResponse($response, $app->resourceName);
        $app->response()->setStatus($this->statusCode);

        // Enable support for JSONP
        if ($app->request()->get('callback')) {
            $body = sprintf('%s(%s)', $app->request()->get('callback'), $body);
        }
        $app->response()->body($body);
    }

    //@TODO get the right resource routes
    //@TODO Add relevant links as well
    public function makeHalResponse($response, $resource)
    {

        // If there is no data to return the HTTP should be 204 Nothing to return
        // Should we add some additional text ??
        if (empty($response)) {
            $this->statusCode = 204;
            return;
        }

        if ($this->isMulti($response)) {
            $halResponse = new Resource($resource);
            $arr = array();

            foreach ($response as $i) {
                //@TODO                                      /*$resource->*/
                $arr[] = new Resource($resource, $i);
            }

            foreach ($arr as $k) {
                $halResponse->setEmbedded('order', $k);
            }
        } else {
            // @TODO get the links as well
            $halResponse = new Resource($resource, array('id' => $response['id']));
            $halResponse->setData($response);
        }

        return $halResponse;
    }

    public function isMulti($arr)
    {
        foreach ($arr as $k) {
            if (is_array($k)) {
                return true;
            }
        }
        return false;
    }
}
