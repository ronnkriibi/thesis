<?php
/**
 * Created by PhpStorm.
 * User: rolpa
 * Date: 1.10.14
 * Time: 13:02
 */

namespace roland\Tool;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Slim\Slim;

abstract class AbstractResource
{

    /**
     * @var \Doctrine\ORM\EntityManager;
     */
    private $entityManger = null;

    protected $app;

    protected $resourceName;

    public function __construct()
    {
        $this->app = Slim::getInstance();
        $this->setResourceName();
        $this->dispatch($this->app->request->getMethod());
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManger()
    {
        if (null === $this->entityManger) {
            $this->entityManger = $this->createEntityManger();
        }
        return $this->entityManger;
    }

    /**
     * @return EntityManager
     */
    public function createEntityManger()
    {
        $path = array('roland/entities'); // Path to entity mapper. I think I want to use YAML Right now using PHP
        $devMode = $this->getMode(); // This should use the app MODE!!!
        $config = Setup::createAnnotationMetadataConfiguration($path, $devMode);

        $connectionOptions = array(
            'driver' => 'pdo_mysql',
            'dbname' => 'cupofcoffee',
            'user' => 'root',
            'password' => ''
        );

        return EntityManager::create($connectionOptions, $config);
    }

    /**
     * Gets the mode from the Slim app
     * @return bool
     */
    protected function getMode()
    {
        return ($this->app->getMode() == 'development' ? true : false);
    }

    public function fetchAll()
    {
        $this->app->render(405, array('Method Not found'));
    }

    public function fetchOne($id)
    {
        $this->app->render(405, array('Method Not found'));
    }

    public function update($id, $data)
    {
        $this->app->render(405, array('Method not found'));
    }

    public function create()
    {
        $this->app->render(405, array('Method not found'));
    }

    public function patch($id, $data)
    {
        $this->app->render(405, array('Method not found'));
    }

    public function option()
    {
        //Return supported media type and allowed http methods
        $this->app->render('400', array('Method not found'));
    }

    public function getResourceName()
    {
        return $this->resourceName;
    }

    private function setResourceName()
    {
        if (null === $this->resourceName) {
            throw \Slim\Exception('Resource name undefined');
        }
        $this->app->resourceName = $this->getResourceName();
    }

    public function getRequest()
    {
        return $this->app->request();
    }

    //@TODO
    // NEEDS a lot of testing
    private function getIdentifier()
    {
        $uriPattern = $this->app->router->getCurrentRoute()->getPattern();
        $params = $this->app->router->getCurrentRoute()->getParams();
        $id = null;
        if (!preg_match_all('/:(\w*)/', $uriPattern, $matches)) {
            return false;
        };
        //@TODO One resource can't have more than one identifier
        foreach ($matches as $match) {
            if (array_key_exists($match[0], $params)) {
                $id = $params[$match[0]];
                break;
            }
        }
        if (!$id) {
            return false;
        }
        return $id;
    }

    private function dispatch($method)
    {
        /**
         * echo $this->app->request->getPath();
         * $request = $this->app->request->getPath();
         */

        $id = false;

        switch ($method) {
            case 'GET':
                $id = $this->getIdentifier();

                if ($id !== false) {
                    $action = 'fetchOne';
                    break;
                }
                $action = 'fetchAll';
                break;

            default:
                $this->app->render('405', array('Method not defined'));
        }

        if ($id) {
            $data = call_user_func(array($this, $action), $id);
        } else {
            $data = call_user_func(array($this, $action));
        }

        $this->app->render(200, array($data));
    }
}
