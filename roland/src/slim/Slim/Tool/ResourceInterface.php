<?php
/**
 * Created by PhpStorm.
 * User: rolpa
 * Date: 7.10.14
 * Time: 14:08
 */
namespace roland\Tool;

interface ResourceInterface
{

    public function fetchOne($id);

    public function fetchAll();

    public function create();

    public function update();

    public function patch();

    public function option();
}
