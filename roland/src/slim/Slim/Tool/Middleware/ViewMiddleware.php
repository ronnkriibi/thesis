<?php
/**
 * Created by PhpStorm.
 * User: rolpa
 * Date: 3.10.14
 * Time: 13:39
 */

namespace roland;

use Slim\Middleware;
use Slim\Slim;

class ViewMiddleware extends Middleware
{

    /**
     * @param bool $showStack
     */
    public function __construct($showStack = false)
    {
        $app = Slim::getInstance();
        /**
         * Apparently in the slim application debug mode has to be set false otherwise it will output the
         * "pretty exception"
         * @TODO Makes sure that previous argument is true and there is no other work around
         * @link https://github.com/needcaffeine/slim-api-extras/blob/master/Middleware/ApiMiddleware.php
        */
        $app->config('debug', false);




        $app->error(function (\Exception $e) use ($app, $showStack) {
            $exception = array(
                'notifications' => array($e->getMessage())
            );
            if ($showStack === true) {
                $exception['stack_trace'] = $e->getTrace();
            }
            print_r($exception);
            die;
            //$app->render(500, $exception);
        });

        // @TODO not working properly right now
        $app->notFound(function () use ($app) {
            $app->halt(
                404,
                json_encode(
                    array(
                          'message' => 'Not valid route',
                          'status_code' => 404
                          )
                )
            );
        });
    }

    /**
     *  Call next middleware
     */
    public function call()
    {
        $this->next->call();
    }
}
