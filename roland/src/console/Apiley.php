<?php namespace Restifarian\console;

/**
 * Created by PhpStorm.
 * User: rolpa
 * Date: 22.01.15
 * Time: 16:15
 */

use Restifarian\console\Console\TestCommand;
use Symfony\Component\Console\Application;


class Apiley extends Application
{

    /**
     *  Applcaiton base path
     *
     * @var string
     */
    private $appPath;

    public function __construct($basPath)
    {
        $this->appPath = $basPath;
        parent::__construct();
    }

    public function getDefaultCommands()
    {
        $defaultCommands = parent::getDefaultCommands();
        $defaultCommands[] = new TestCommand($this->appPath);

        return $defaultCommands;
    }
}
