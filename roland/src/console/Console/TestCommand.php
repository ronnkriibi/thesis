<?php namespace Restifarian\console\Console;

use Raml\Parser;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;
use Restifarian\console\Console\SlimCompiler;

class TestCommand extends Command
{
    /**
     * Supported frameworks
     * @var array
     */
    private $options = array('Slim', 'Silex', 'Laravel');

    private $basePath;

    public function __construct($basePath)
    {
        $this->basePath = $basePath;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('generate')
            ->setDescription('Read raml file and generate routes and resources')
            ->addArgument('file', InputArgument::REQUIRED, 'File that will be parsed');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $file = $input->getArgument('file');

        if (!file_exists($file)) {
            throw new \InvalidArgumentException('File could not be found !');
        }

        $parseResult = $this->parseRamlFromCmd($file);

        $title = $parseResult->getTitle();
        $version = $parseResult->getVersion();
        $baseUri = $parseResult->getBaseUri();

        $text =  'File ' . $file . ' parsed. Generating API for: '
            . $title . ' with base uri: '
            . $baseUri . ' with version nr: '
            . $version;

        $output->writeln($text);

        /**
         * Ask user some questions in order to produce right output
         */
        $helper = $this->getHelper('question');

        $questionFramwork = new ChoiceQuestion(
            'Please choose what framework are you using (default Slim)',
            $this->options,
            0
        );
        $options = $this->options;
        $questionFramwork->setValidator(
            function ($answer) use ($options) {
                if (!isset($options[$answer])) {
                    throw new \InvalidArgumentException(sprintf('Invalid input %s', $answer));
                } elseif ($options[$answer] !== 'Slim') {
                    throw new \InvalidArgumentException('This generator does not support: '. $options[$answer]);
                }
                return $options[$answer];
            }
        );

        $framework = $helper->ask($input, $output, $questionFramwork);

        $output->writeln('You have just selected: '. $framework);

        /**
        This part might not be needed
        $whereIsBaseFile = new Question('Where is your base file for ' . $framework . ': ');
        $baseFile = $helper->ask($input, $output, $whereIsBaseFile);

        if (empty($baseFile)) {
            throw new \InvalidArgumentException('Base file is required for ' . $framework);
        }

        //@TODO get the base path some how
        $basePath = 'D:\dev\thesis/';
        $fullPath = $basePath . $baseFile;
        $output->writeln('Looking in ' . $fullPath);

        if (!file_exists($fullPath)) {
            throw new \InvalidArgumentException('File could not be found in ' . $fullPath);
        }

        $output->writeln('Found it');
        */
        $whereTo = new Question('Which directory do you want to save the output index.php file :');
        $whereTo->setValidator(
            function ($answer) {
                if (empty($answer)) {
                    return;
                } elseif (!file_exists($answer)) {
                    throw new \InvalidArgumentException($answer . ' Directory does not exists');
                } elseif (!is_writable($answer)) {
                    throw new \InvalidArgumentException($answer . ' Directory is not writable');
                }
                return $answer;
            }
        );
        $whereTo = $helper->ask($input, $output, $whereTo);

        // Set base path to it
        if (empty($whereTo)) {
            $whereTo = $this->basePath;
        }

        $output->writeln('Starting code generator');


        //@TODO as what kind of structure he or she wants
        //@TODO add hyperlinks

        //@TODO what about API versioning

        /**
         * @TODO It would be cool if it showed progress. In orderd for doing that we should add:
         *  $progress = $this->getHelper('progress');
         *  $progress->start($output, 50)
         *  $progress->advance();
         *  $progress->finish();
         */

        switch ($framework) {
            case 'Slim':
                $compiler = new SlimCompiler($parseResult, array('regular' => true, 'output' => $whereTo));
                break;
            default:
                break;
        }
        $compiler->buildStructure();

        $output->writeln('Done');
    }

    /**
     * Parses the file that is given by the user
     * @param $file
     * @return \Raml\ApiDefinition
     */
    private function parseRamlFromCmd($file)
    {
        $parser = new Parser();
        $appStructure = $parser->parse($file, true);
        return $appStructure;
    }
}
