<?php
/**
 * Created by PhpStorm.
 * User: rolpa
 * Date: 28.01.15
 * Time: 11:49
 */

namespace Restifarian\console\Console;

class SlimCompiler implements CompilerInterface
{
    /**
     * @var
     */
    private $apiDefinition;

    /**
     * @var
     */
    private $options;

    /**
     * @var
     */
    private $templates;

    /**
     * @var
     */
    private $routeText;

    /**
     * @param \Raml\ApiDefinition $apiDefinition
     * @param $options
     * @TODO Two use cases generate skeleton and mock's;
     * @TODO should make this as an object to. Should it add resources should it add example response
     * should it generate a resource object?
     * @TODO add comments to generated routes!
     */
    //@TODO add other features as well like params and stuff
    public function __construct(\Raml\ApiDefinition $apiDefinition, $options)
    {
        $this->options       = $options;
        $this->apiDefinition = $apiDefinition;
    }

    public function buildStructure()
    {
        $this->loadTemplates();
        if ($this->options['regular'] === true) {
            $resources = $this->apiDefinition->getResources();
            $this->regularSlimStructure($resources, $this->templates['route']);

            $output = preg_replace("/€ROUTES€/i", $this->routeText, $this->templates['main_template']);
            file_put_contents($this->options['output'] . '/index3.php', print_r($output, true), FILE_APPEND);
        }
    }

    private function regularSlimStructure($resource, $sampleText)
    {
        if (is_array($resource)) {
            foreach ($resource as $subs) {
                if ($subs instanceof \Raml\Resource) {
                    //For each method we have to produce the code
                    foreach ($subs->getMethods() as $method) {
                        if ($method instanceof \Raml\Method) {
                            $methodName = $method->getType();
                            $sample = preg_replace("/€MEHTOD€/i", strtolower($methodName), $sampleText);

                            $uri = $subs->getUri();
                            $params = '';
                            //Find all the params from uri if none is found the just add the uri
                            if (preg_match_all("/\{(.*?)\}/", $uri, $matches)) {
                                //Replace all the params from uri
                                foreach ($matches[0] as $k => $match) {
                                    $uri = preg_replace("/" . $match . "/", ':' . $matches[1][$k], $uri);
                                    $params .= '$' . $matches[1][$k] . ', ';

                                }
                                $sample = preg_replace("/€URI€/i", $uri, $sample);
                            } else {
                                $sample = preg_replace("/€URI€/i", $uri, $sample);
                            }

                            //remove last comma
                            $params = rtrim($params, ', ');
                            //Substitute params
                            $sample = preg_replace("/€PARAMS€/i", $params, $sample);

                            $desc = $method->getDescription();
                            if (empty($desc)) {
                                $desc = 'Hello world';
                            }

                            $sample = preg_replace("/€DESCRIPTION€/i", $desc, $sample);

                            // Write to the file
                            if (!empty($sample)) {
                                $this->routeText .= $sample . PHP_EOL . PHP_EOL;
                            }
                        }
                    }

                    return $this->regularSlimStructure($subs->getResources(), $this->templates['route']);
                }
            }
        }
        return $resource;
    }

    private function loadTemplates()
    {
        if ($this->options['regular'] === true) {
            $this->templates['main_template'] = file_get_contents(__DIR__ . '/Templates/slim_index_template');
            $this->templates['route'] = file_get_contents(__DIR__ . '/Templates/slim_regular_route');

        }
        /**
         *
        if ($this->options['add_resources'] === true) {
            //@TODO add resources
        }
         */
    }
}
