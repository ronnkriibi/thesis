<?php
/**
 * Created by PhpStorm.
 * User: rolpa
 * Date: 28.01.15
 * Time: 14:01
 */

namespace Restifarian\console\Console;

interface CompilerInterface
{
    public function buildStructure();
}
