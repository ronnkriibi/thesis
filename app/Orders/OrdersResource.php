<?php
/**
 * Created by PhpStorm.
 * User: rolpa
 * Date: 1.10.14
 * Time: 14:12
 */

use Slim\Slim;
use entities\Orders;
use roland\AbstractResource;


class OrdersResource extends AbstractResource
{

    /**
     *  List of values that we want to return within this Resource
     * @var array
     */
    protected $returnableValues = array('id', 'coffee', 'cost');

    public $resourceName = 'orders';

    public function fetchOne($id)
    {
        $order = $this->getEntityManger()->find('entities\Orders', $id);
        // @TODO This requires array right now but it really shouldn't
        if($order instanceof Orders) {
            $order = $this->makeReturnArray($order);
        } else {
            $order = array();
        }

        return $order;
    }

    public function fetchAll()
    {
        $orders = $this->getEntityManger()->getRepository('entities\Orders')->findAll();
        $orders = array_map(
            function($order) {
                return $this->makeReturnArray($order);
            },
            $orders
        );
        return $orders;
    }


    //@TODO This should construct the entity
    public function create()
    {
        //@TODO How to make this work without invoking the $app!!!


        $order = new Orders();

        //@TODO This should be moved to place that will do it automatically; Also content negotiator is needed
        $data = json_decode($this->getRequest());

        foreach($data as $k => $i) {
            $order->setKeys($k, $i);
        }

        $this->getEntityManger()->persist($order);
        $this->getEntityManger()->flush($order);

        //@TODO should return Cost
        return $order->getValue('id');
    }

    // This should be PUT
    public function update($id, $data) {

    }


    private function makeReturnArray(Orders $order) {
        $arr = [];
        $metaData = $this->getEntityManger()->getClassMetadata('entities\Orders');
        foreach($metaData->columnNames as $columnName) {
            if(!in_array($columnName, $this->returnableValues)) {
                continue;
            }
            $arr[$columnName] = $order->getValue($columnName);
        }
        return $arr;
    }
}