<?php
/**
 * Created by PhpStorm.
 * User: rolpa
 * Date: 1.10.14
 * Time: 13:32
 */

namespace entities;

use entities;
use Doctrine\ORM\Mapping;

/**
 * @Entity
 * @Table(name="`order`")
 * @comment This is a bad example because order is reserved word in MYSql
 */
class Orders
{


    /**
     * @var int
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @var string
     * @Column(type="string", length=65)
     */
    protected $coffee;

    /**
     * @var string
     * @Column(type="string", length=65)
     */
    protected $additions; //@TODO this should be embedded resource

    /**
     * @var float
     * @Column(type="float")
     */
    protected $cost;

    public function setKeys($key, $value) {
        if(!property_exists($this, $key)) {
            return;
        }
        $this->$key = $value;
    }

    public function getValue($key) {
        if(!property_exists($this, $key)) {
            return;
        }
        return $this->$key;
    }

    public function getId() {
        return $this->id;
    }

    public function getCoffee() {
        return $this->coffee;
    }

    public function getAdditions() {
        return $this->additions;
    }

    public function getCost() {
        return $this->cost;
    }
}