<?php
/**
 * Created by PhpStorm.
 * User: rolpa
 * Date: 1.10.14
 * Time: 10:21
 */

include dirname(__DIR__) . '/vendor/autoload.php';
include dirname(__DIR__) . '/roland/vendor/autoload.php';



$parser = new \Raml\Parser();
$appStructure = $parser->parse(dirname(__DIR__) . '/eventAPI/eventlog.raml', true);

$compiler = new \Restifarian\console\Console\SlimCompiler($appStructure, array('regular' => true, 'output' => dirname(__DIR__)));

$compiler->buildStructure();

die;
use Slim\Slim;

$app = new Slim();

$app->view(new RestFulView($app->config('debug')));
$app->add(new ViewMiddleware($app->config('debug')));


$routes = new Route();

foreach ($routes->addRoutes() as $route) {
    $app->router->map($route);
}

$app->run();
